Simple betting dapp build on top of ethereum network ( testnet : Ropsten )

## Prerequisites
- Node.js https://nodejs.org/en/download/ 
- npm if not installed with Node.js https://www.npmjs.com/get-npm
- MetaMask https://metamask.io/
- This project

### Create MetaMask Account

- After downloading MetaMask click on extension icon and create new account
- Change network from main net to Ropsten
- Click deposit and click get Ether you will be redirected to faucet and click request ether ( this will deposit some ETH into you account)
IMPORTANT
- Click on  circle in top right corner of MetaMask app
- Click settings -> Security & Privacy -> Turn off Privacy mode

### Install

- After downloading project open console and enter folder 
- Type "npm install" it will download all needed dependencies ( BTW you can find them inside "package.json file")

### Run

- To run enter project using console 
- Type "npm start" it should open new website running on http://localhost:3000/
- After performing some action MetaMask will open mini window to confirm transactions
- You can check out your transactions on https://ropsten.etherscan.io/address/0xFF5964657988D379FABCA35c5B85fD8086c24Aa4

### Deploy own copy

Since i created contract only i can change "Max Bets" if you want to have this option you need to create copy of it .

- First enter you project folder using console 
- Type "npm install truffle -g "
- Type "npm install truffle-contract "
- Type "npm install truffle-wallet-provider "
- Create account on Infura https://infura.io/dashboard
- Login and click "Create New Project"
- Name it and Click "View Project"
- Under "Keys" field change endpoint to Ropsten
- Copy Address and replace value inside truffle.js file Row 31 ( keep "https://")
- Now open MetaMask (you should have some ETH) click on three dots next to your address click "Account Details" and export your private key
- Copy your private key and replace value inside truffle.js file Row 12 and save File ( don’t steal my fake ETH ;) )
- Open folder inside console
- Type "truffle compile"
- Type "truffle migrate --network ropsten"
- Wait for a couple of blocks
- After deployment of smartcontract
- Replace files inside /src/contracts using files inside /build/contracts
- Open console and run app " npm start"
 
That's it thanks


