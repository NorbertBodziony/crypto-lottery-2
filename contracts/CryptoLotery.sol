pragma solidity >0.4.99;

contract CryptoLotery {
    
    uint8 public maxBets=2;
    uint public currentBets =0;
    uint public pool=0;
    uint public lastRandom=0;
    uint nonce=10;
    
    address public lastWinner;
    address public owner;
    
    uint[] public bets;
    address payable[] public players;
    
    event newLotteryCount(address _address,uint _newCount);
    event newBet(address _address,uint _bet);
    event gameOver(address _address,uint _price);
    
    constructor() public payable
    {
        owner=msg.sender;
    }
     function getBalance() public view returns (uint) {
        return address(this).balance;
    }
    modifier onlyOwner {
        require(msg.sender==owner);
        _;
    }
    
    function setLotteryCount(uint8 _newMaxBets) onlyOwner public{
        require(currentBets<_newMaxBets);
        require(_newMaxBets<2**8);
        maxBets=_newMaxBets;
        emit newLotteryCount(msg.sender,_newMaxBets);
    }
 
     function addPlayer() internal{
         players.push(msg.sender);
     }
     function addBet() internal
     {
         bets.push(msg.value);
         pool+=msg.value;
     }

    function placeBet()public payable{
        require(msg.value > .0000001 ether);
        addPlayer();
        addBet();
        increasecurrentBets();
        checkGame();
      emit newBet(msg.sender,msg.value);
        
    }
    function increasecurrentBets() internal{
        currentBets++;
    }
     function resetcurrentBets() internal{
        currentBets=0;
    }
    function checkGame() internal
    {
        if(currentBets==maxBets){
            getWinner(random());
            resetcurrentBets();
            delete players;
            delete bets;
            pool=0;
        }
    }
    function random() internal returns (uint) {
    uint randomnumber = uint(keccak256(abi.encodePacked(now, msg.sender, nonce))) % pool;
    nonce++;
    return randomnumber;
}
    function getWinner(uint _random) internal {
        uint temp=0;
        uint i=0;
        while(true)
        {
            
            temp+=bets[i];
            if(temp>_random)
            {
                players[i].transfer((pool));
                lastRandom=_random;
                lastWinner=players[i];
                emit gameOver(players[i],pool);
                break;
            }
            i++;
    
        }
    }
    
}
