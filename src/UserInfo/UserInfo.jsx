
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';

import './UserInfo.css';


class UserInfo extends Component {
  getBet() {
    let bet = 0;
    for (let i = 0; i < this.props.players.length; i += 1) {
      if (this.props.players[i] === this.props.address) {
        bet += parseInt(this.props.bets[i], 10);
      }
    }
    return bet / this.props.weiToEth;
  }

  render() {
    const poolValue = () => {
      if (parseInt(this.props.pool, 10) === 0) {
        return 1;
      }
      return this.props.pool / this.props.weiToEth;
    };
    return (


      <React.Fragment>
        <div className="UserInfo">
          {this.props.balance !== -1 ? (
            <div>
              <div>
                Current Balance :
                {' '}
                {this.props.balance / this.props.weiToEth}
                {' '}
                ETH
              </div>
              <div>
                Current Bet :
                {' '}
                {this.getBet()}
                {' '}
                ETH
              </div>
              <div>
                Current Win Chance :
                {' '}
                {parseFloat((this.getBet() / poolValue()) * 100).toPrecision(3)}
                %
              </div>
            </div>
          ) : (
            <h1>Please Log Into MetaMask</h1>
          )}
        </div>
      </React.Fragment>


    );
  }
}
function mapStateToProps(state) {
  return {
    balance: state.balance,
    pool: state.pool,
    players: state.players,
    bets: state.bets,
    weiToEth: state.weiToEth,
    address: state.address,
  };
}
export default connect(mapStateToProps)(UserInfo);
