/* eslint-disable no-return-await */


export async function getEventBet(BettingInstance) {
  return await BettingInstance.newBet.call();
}

export async function getEventWinner(BettingInstance) {
  return await BettingInstance.gameOver.call();
}

export async function getEventLotteryCount(BettingInstance) {
  return await BettingInstance.newLotteryCount.call();
}
