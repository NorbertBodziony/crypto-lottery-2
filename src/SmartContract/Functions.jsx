/* eslint-disable no-await-in-loop */
/* eslint-disable global-require */
/* eslint-disable no-return-await */

import Contract from '../contracts/CryptoLotery.json';

export async function getBalance(web3) {
  const accounts = await web3.eth.getAccounts();
  if (accounts[0] === undefined) {
    return -1;
  }
  const balance = await web3.eth.getBalance(accounts[0]);
  return balance;
}
export async function getAddress(web3) {
  const accounts = await web3.eth.getAccounts();
  return accounts[0];
}
export async function getContractInstance(web3) {
  const contract = require('truffle-contract');
  const Betting = contract(Contract);
  Betting.setProvider(web3.currentProvider);
  const BettingInstance = await Betting.deployed();
  return BettingInstance;
}
export async function getOwner(BettingInstance) {
  return await BettingInstance.owner.call();
}

export async function getMaxBets(BettingInstance) {
  const maxBets = await BettingInstance.maxBets.call();
  return maxBets.toString();
}
export async function getCurrentBets(BettingInstance) {
  const currentBets = await BettingInstance.currentBets.call();
  return currentBets.toString();
}

export async function getLastRandom(BettingInstance) {
  const lastRandom = await BettingInstance.lastRandom.call();
  return lastRandom.toString();
}
export async function getWinner(BettingInstance) {
  const lastWinner = await BettingInstance.lastWinner.call();
  return lastWinner.toString();
}
export async function getPool(BettingInstance) {
  const pool = await BettingInstance.pool.call();
  return pool.toString();
}
export async function getPlayers(web3, currentBets, BettingInstance) {
  const players = [];
  for (let i = 0; i < currentBets; i += 1) {
    const p = await BettingInstance.players.call(web3.utils.toBN(i));
    players.push(p);
  }
  return players;
}
export async function getBets(web3, currentBets, BettingInstance) {
  const bets = [];
  for (let i = 0; i < currentBets; i += 1) {
    const p = await BettingInstance.bets.call(web3.utils.toBN(i));
    bets.push(p.toString());
  }
  return bets;
}
