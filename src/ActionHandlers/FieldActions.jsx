/* eslint-disable import/prefer-default-export */
export const handleBetSizeChange = value => ({
  type: 'SET_BETSIZE',
  payload: value,
});
export const handleMaxBetsChange = value => ({
  type: 'SET_MAXBETSVAR',
  payload: value,
});
