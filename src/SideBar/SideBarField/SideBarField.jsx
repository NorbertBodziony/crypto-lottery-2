/* eslint-disable import/prefer-default-export */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';

import './SideBarField.css';

export const SideBarField = (message, value) => (
  <div className="SideBarField">
    <div>{message}</div>
    <div>{value}</div>
  </div>
);
