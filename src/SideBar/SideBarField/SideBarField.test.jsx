/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import SideBarField from './SideBarField';

it('renders without crashing', () => {
  shallow(<SideBarField />);
});


it('renders correctly', () => {
  const sideBarField = renderer
    .create(<SideBarField message="test" value="test" />)
    .toJSON();
  expect(sideBarField).toMatchSnapshot();
});
