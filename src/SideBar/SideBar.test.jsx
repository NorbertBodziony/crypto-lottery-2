/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import SideBar from './SideBar';

it('renders without crashing', () => {
  shallow(<SideBar />);
});

it('renders correctly', () => {
  const sideBar = renderer
    .create(
      <SideBar
        maxBets="test"
        currentBets="test"
        pool="test"
        winner="test"
        lastRandom="test"
      />,
    )
    .toJSON();
  expect(sideBar).toMatchSnapshot();
});
