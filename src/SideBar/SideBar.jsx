/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SideBarField } from './SideBarField/SideBarField';
import './SideBar.css';


class SideBar extends Component {
  render() {
    const pool = `${this.props.pool / this.props.weiToEth} ETH`;
    return (
      <div className="SideBar">
        {SideBarField('Max Bets : ', this.props.maxBets)}
        {SideBarField('Bets : ', this.props.currentBets)}
        {SideBarField('Pool :', pool)}
        {SideBarField('Last Winner :', this.props.winner)}
        {SideBarField('Last Random Number :', this.props.lastRandom)}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    maxBets: state.maxBets,
    currentBets: state.currentBets,
    pool: state.pool,
    winner: state.winner,
    lastRandom: state.lastRandom,
    weiToEth: state.weiToEth,
  };
}
export default connect(mapStateToProps)(SideBar);
