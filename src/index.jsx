/* eslint-disable no-underscore-dangle */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {
  address,
  balance,
  maxBets,
  currentBets,
  pool,
  owner,
  lastRandom,
  winner,
  players,
  bets,
  showAlert,
  weiToEth,
  message,
  betSize,
  maxBetsVar,
} from './Reducers/Reducers';

const game = combineReducers({
  address,
  balance,
  maxBets,
  currentBets,
  pool,
  owner,
  lastRandom,
  winner,
  players,
  bets,
  showAlert,
  weiToEth,
  message,
  betSize,
  maxBetsVar,

});

const store = createStore(
  game,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

ReactDOM.render(<Provider store={store}><App store={store} /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
