/* eslint-disable react/no-unused-state */
/* eslint-disable react/prop-types */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-console */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-access-state-in-setstate */

/* eslint-disable max-len */
import React, { Component } from 'react';
import './App.css';
import getWeb3 from './utils/getWeb3';
import SideBar from './SideBar/SideBar';
import AlertBox from './AlertBox/AlertBox';
import UserInfo from './UserInfo/UserInfo';
import PlaceBet from './Menu/PlaceBet';
import ChangeMaxBet from './Menu/ChangeMaxBets';
import { ContractContext } from './ContractContext';
import {
  getBalance,
  getOwner,
  getMaxBets,
  getCurrentBets,
  getLastRandom,
  getBets,
  getPlayers,
  getWinner,
  getPool,
  getAddress,
  getContractInstance,
} from './SmartContract/Functions';
import { getEventBet, getEventLotteryCount, getEventWinner } from './SmartContract/Events';


const BigNumber = require('bignumber.js');


class App extends Component {
  async componentDidMount() {
    const { store } = this.props;
    const web = await getWeb3;
    console.log(ContractContext);
    const { web3 } = web;
    const contractInstance = await getContractInstance(web3);

    const [balanceValue, ownerValue, maxBetsValue, currentBetsValue, lastRandomValue, winnerValue, poolValue, addressValue] = await Promise.all([
      getBalance(web3),
      getOwner(contractInstance),
      getMaxBets(contractInstance),
      getCurrentBets(contractInstance),
      getLastRandom(contractInstance),
      getWinner(contractInstance),
      getPool(contractInstance),
      getAddress(web3),
    ]);
    const [playersValue, betsValue, eventBet, eventLoteryCount, eventWinner] = await Promise.all([
      getPlayers(web3, currentBetsValue, contractInstance),
      getBets(web3, currentBetsValue, contractInstance),
      getEventBet(contractInstance),
      getEventLotteryCount(contractInstance),
      getEventWinner(contractInstance),
    ]);
    this.setState({
      web3,
      contractInstance,
    });
    store.dispatch({ type: 'SET_BALANCE', payload: balanceValue });
    store.dispatch({ type: 'SET_OWNER', payload: ownerValue });
    store.dispatch({ type: 'SET_MAXBETS', payload: maxBetsValue });
    store.dispatch({ type: 'SET_CURRENTBETS', payload: currentBetsValue });
    store.dispatch({ type: 'SET_LASTRANDOM', payload: lastRandomValue });
    store.dispatch({ type: 'SET_WINNER', payload: winnerValue });
    store.dispatch({ type: 'SET_POOL', payload: poolValue });
    store.dispatch({ type: 'SET_ADDRESS', payload: addressValue });
    store.dispatch({ type: 'SET_PLAYERS', payload: playersValue });
    store.dispatch({ type: 'SET_BETS', payload: betsValue });

    // ////EVENTS//////


    eventBet.on('data', (event) => {
      this.handleEventBet(event.args[0], BigNumber(event.args[1]).toString());
    });

    eventLoteryCount.on('data', (event) => {
      // console.log(event)
      this.handleEventLotteryCount(BigNumber(event.args[1]).toString());
    });
    eventWinner.on('data', (event) => {
      this.handleEventWinner(event.args[0], BigNumber(event.args[1]).toString());
    });
  }

  async handleEventBet(acc, value) {
    const { store } = this.props;
    const state = store.getState();
    if (parseInt(state.currentBets, 10) + 1 !== parseInt(state.maxBets, 10)) {
      const [balanceValue, poolValue, currentBetsValue] = await Promise.all([
        getBalance(this.state.web3),
        getPool(this.state.contractInstance),
        getCurrentBets(this.state.contractInstance),
      ]);
      const [playersValue, betsValue] = await Promise.all([
        getPlayers(this.state.web3, currentBetsValue, this.state.contractInstance),
        getBets(this.state.web3, currentBetsValue, this.state.contractInstance),
      ]);
      const messageValue = () => (
        <div>
          <h3>
            New Bet By :
            {acc}
          </h3>
          <h3>
            Size :
            {' '}
            {(value / state.weiToEth).toString()}
            {' '}
            ETH
          </h3>
        </div>
      );
      store.dispatch({ type: 'SET_PLAYERS', payload: playersValue });
      store.dispatch({ type: 'SET_BETS', payload: betsValue });
      store.dispatch({ type: 'SET_BALANCE', payload: balanceValue });
      store.dispatch({ type: 'SET_CURRENTBETS', payload: currentBetsValue });
      store.dispatch({ type: 'SET_POOL', payload: poolValue });
      store.dispatch({ type: 'SET_MESSAGE', payload: messageValue() });
      store.dispatch({ type: 'SHOW_ALERT' });
      setTimeout(() => {
        store.dispatch({ type: 'HIDE_ALERT' });
        console.log('New BET ');
      }, 8000);
    }
  }

  async handleEventWinner(acc, value) {
    const { store } = this.props;
    const state = store.getState();
    const [balanceValue, poolValue, currentBetsValue, winnerValue, lastRandomValue] = await Promise.all([
      getBalance(this.state.web3),
      getPool(this.state.contractInstance),
      getCurrentBets(this.state.contractInstance),
      getWinner(this.state.contractInstance),
      getLastRandom(this.state.contractInstance),
    ]);
    const [playersValue, betsValue] = await Promise.all([
      getPlayers(this.state.web3, currentBetsValue, this.state.contractInstance),
      getBets(this.state.web3, currentBetsValue, this.state.contractInstance),
    ]);

    const messageValue = () => (
      <div>
        <h3>
          New Winner:
          {acc}
        </h3>
        <h3>
          Prize :
          {' '}
          {(value / state.weiToEth).toString()}
          {' '}
          ETH
        </h3>
      </div>
    );
    store.dispatch({ type: 'SET_BALANCE', payload: balanceValue });
    store.dispatch({ type: 'SET_CURRENTBETS', payload: currentBetsValue });
    store.dispatch({ type: 'SET_LASTRANDOM', payload: lastRandomValue });
    store.dispatch({ type: 'SET_WINNER', payload: winnerValue });
    store.dispatch({ type: 'SET_POOL', payload: poolValue });
    store.dispatch({ type: 'SET_PLAYERS', payload: playersValue });
    store.dispatch({ type: 'SET_BETS', payload: betsValue });
    store.dispatch({ type: 'SET_MESSAGE', payload: messageValue() });
    store.dispatch({ type: 'SHOW_ALERT' });

    setTimeout(() => {
      store.dispatch({ type: 'HIDE_ALERT' });
      console.log('New Winner ');
    }, 8000);
  }

  async handleEventLotteryCount(value) {
    const { store } = this.props;
    const maxBetsValue = await getMaxBets(this.state.contractInstance);

    const messageValue = () => (
      <div>
        <h3>
          New Max Bets :
          {value}
        </h3>
      </div>
    );

    store.dispatch({ type: 'SET_MAXBETS', payload: maxBetsValue });
    store.dispatch({ type: 'SET_MESSAGE', payload: messageValue() });
    store.dispatch({ type: 'SHOW_ALERT' });
    setTimeout(() => {
      store.dispatch({ type: 'HIDE_ALERT' });
    }, 8000);
  }


  render() {
    return (

      <div className="App">
        <header className="App-header">
          <div className="Title"> Crypto Lotery</div>
          <div className="Screen">
            <ContractContext.Provider value={this.state}>
              <SideBar />
              <div className="Main">
                <AlertBox />
                <UserInfo />
                <PlaceBet />
                <ChangeMaxBet />
              </div>
            </ContractContext.Provider>
          </div>
        </header>
      </div>

    );
  }
}

export default App;
