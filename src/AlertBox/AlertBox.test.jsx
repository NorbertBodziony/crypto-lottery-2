/* eslint-disable no-undef */
import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import AlertBox from './AlertBox';

it('renders without crashing', () => {
  shallow(<AlertBox />);
});

it('renders correctly', () => {
  const alertBox = renderer
    .create(<AlertBox message="test" isOpen />)
    .toJSON();
  expect(alertBox).toMatchSnapshot();
});
