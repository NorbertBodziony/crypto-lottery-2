/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';
import { connect } from 'react-redux';
import './AlertBox.css';

class AlertBox extends Component {
  render() {
    return (
      <div className="AlertBox">
        <Alert variant="danger" show={this.props.showAlert}>
          {this.props.message}
        </Alert>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    message: state.message,
    showAlert: state.showAlert,

  };
}

export default connect(mapStateToProps)(AlertBox);
