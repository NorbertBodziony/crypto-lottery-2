/* eslint-disable no-undef */
import React from 'react';
import { shallow, mount } from 'enzyme';
import ChangeMaxBets from './ChangeMaxBets';

it('renders without crashing', () => {
  shallow(<ChangeMaxBets />);
});

it('should update betSize', () => {
  const component = mount(<ChangeMaxBets />);
  component
    .setState({ showForm: true })
    .find('FormControl')
    .simulate('change', { target: { value: 5 } });

  expect(component.state('maxBets')).toEqual(5);
  component.unmount();
});

it('should change betSize to 2', () => {
  const component = mount(<ChangeMaxBets />);

  component
    .setState({ showForm: true })
    .find('FormControl')
    .simulate('change', { target: { value: -5 } });

  expect(component.state('maxBets')).toEqual(2);
  component.unmount();
});

it('should change change showForm to true', () => {
  const component = mount(<ChangeMaxBets address="test" owner="test" />);

  component
    .find('Button')
    .simulate('click');

  expect(component.state('showForm')).toEqual(true);
  component.unmount();
});

it('should call setLotteryCount function', () => {
  const component = mount(<ChangeMaxBets />);
  const setLotteryCount = jest.fn();
  component.instance().setLotteryCount = setLotteryCount;
  component.instance().forceUpdate();
  component
    .setState({ showForm: true })
    .find('Button')
    .at(1)
    .simulate('click');

  expect(setLotteryCount).toHaveBeenCalled();
  component.unmount();
});
