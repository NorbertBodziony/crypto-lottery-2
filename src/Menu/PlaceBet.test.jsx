/* eslint-disable no-undef */
import React from 'react';
import { shallow, mount } from 'enzyme';
import { PlaceBet } from './PlaceBet';

it('renders without crashing', () => {
  shallow(<PlaceBet />);
});



it('should call placeBet function', () => {
  const component = mount(<PlaceBet />);
  const placeBet = jest.fn();
  component.instance().placeBet = placeBet;
  component.instance().forceUpdate();
  component
    .find('Button')
    .simulate('click');
  expect(placeBet).toHaveBeenCalled();
  component.unmount();
});
