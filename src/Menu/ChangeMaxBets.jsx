/* eslint-disable react/no-access-state-in-setstate */
/* eslint-disable max-len */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable global-require */
import React, { Component } from 'react';
import {
  Button, Alert, InputGroup, Form,
} from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { handleMaxBetsChange } from '../ActionHandlers/FieldActions';
import { ContractContext } from '../ContractContext';
import './ChangeMaxBets.css';

class ChangeMaxBets extends Component {
  constructor() {
    super();
    this.state = {
      showButton: true,
      showForm: false,
      showAlert: false,
    };

    this.checkUserAddress = this.checkUserAddress.bind(this);
    this.showForm = this.showForm.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }


  checkUserAddress() {
    if (this.props.address === this.props.owner) {
      this.setState({
        showForm: true,
        showButton: false,
      });
    } else {
      this.setState({
        showAlert: true,
      });
    }
  }

  showAlert() {
    setTimeout(
      () => {
        this.setState({ showAlert: !this.props.showAlert });
      },
      3000,
    );

    return (
      <div className="CantChangeBetsAlert">
        <Alert variant="warning" show>
          You cant change Max Bets !
        </Alert>
      </div>
    );
  }

  showForm() {
    const { contractInstance, web3 } = this.context;
    const setLotteryCount = () => {
      contractInstance.setLotteryCount(web3.utils.toBN(this.props.maxBetsVar), { from: this.props.address });
    };
    return (
      <div className="ChangeMaxBets">
        Enter New Max Bets:
        <InputGroup>
          <Form.Control
            type="number"
            placeholder="Max Bets"
            aria-describedby="inputGroupPrepend"
            name="username"
            onChange={e => this.props.handleMaxBetsChange(e.target.value)}
            value={this.props.maxBetsVar}
            min="2"
          />
          <Form.Control.Feedback type="invalid">
            {}
          </Form.Control.Feedback>
          <InputGroup.Append>
            <InputGroup.Text id="inputGroupPrepend">Bets</InputGroup.Text>
          </InputGroup.Append>
        </InputGroup>
        <div className="button">
          <Button variant="outline-info" size="lg" onClick={setLotteryCount}>Change </Button>
        </div>
      </div>
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.state.showButton && <Button variant="outline-warning" size="lg" onClick={this.checkUserAddress}> Change Max Bets </Button>}
        {this.state.showForm && this.showForm()}
        {this.state.showAlert && this.showAlert()}
      </React.Fragment>
    );
  }
}
ChangeMaxBets.contextType = ContractContext;
function mapStateToProps(state) {
  return {
    owner: state.owner,
    weiToEth: state.weiToEth,
    address: state.address,
    maxBetsVar: state.maxBetsVar,
  };
}
function mapDispatchToProps(dispach) {
  return bindActionCreators({ handleMaxBetsChange }, dispach);
}
export default connect(mapStateToProps, mapDispatchToProps)(ChangeMaxBets);
