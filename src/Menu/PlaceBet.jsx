/* eslint-disable react/prefer-stateless-function */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import './PlaceBet.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { InputGroup, Form, Button } from 'react-bootstrap';
import { handleBetSizeChange } from '../ActionHandlers/FieldActions';
import { ContractContext } from '../ContractContext';

export class PlaceBet extends Component {
  placeBet=()=> {
    const contractInstance= this.context.contractInstance
    contractInstance.placeBet({
      from: this.props.address,
      value: parseFloat(this.props.betSize * this.props.weiToEth),
    });
  }

  render() {
    
    return (
      <div className="PlaceBet">
            Enter Bet Size :

        <div className="InputField">
          <InputGroup>
            <Form.Control
              type="number"
              placeholder="Bet Size"
              aria-describedby="inputGroupPrepend"
              name="username"
              value={this.props.betSize}
              onChange={e => this.props.handleBetSizeChange(e.target.value)}
              min="0"
            />
            <Form.Control.Feedback type="invalid">{}</Form.Control.Feedback>
            <InputGroup.Append>
              <InputGroup.Text id="inputGroupPrepend">ETH</InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
        </div>
        <div className="button">
          <Button variant="outline-danger" size="lg" onClick={this.placeBet}>
                Place Bet
            {' '}
          </Button>
        </div>
      </div>

    );
  }
}
PlaceBet.contextType = ContractContext;
function mapStateToProps(state) {
  return {
    weiToEth: state.weiToEth,
    address: state.address,
    betSize: state.betSize,
  };
}
function mapDispatchToProps(dispach) {
  return bindActionCreators({ handleBetSizeChange }, dispach);
}
export default connect(mapStateToProps, mapDispatchToProps)(PlaceBet);
