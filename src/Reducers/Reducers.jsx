/* eslint-disable import/prefer-default-export */

export const balance = (state = 0, action) => {
  switch (action.type) {
    case 'SET_BALANCE': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};

export const owner = (state = {}, action) => {
  switch (action.type) {
    case 'SET_OWNER': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};

export const maxBets = (state = 0, action) => {
  switch (action.type) {
    case 'SET_MAXBETS': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};

export const lastRandom = (state = 0, action) => {
  switch (action.type) {
    case 'SET_LASTRANDOM': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};

export const currentBets = (state = 0, action) => {
  switch (action.type) {
    case 'SET_CURRENTBETS': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
export const winner = (state = 0, action) => {
  switch (action.type) {
    case 'SET_WINNER': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};

export const pool = (state = 0, action) => {
  switch (action.type) {
    case 'SET_POOL': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
export const address = (state = 0, action) => {
  switch (action.type) {
    case 'SET_ADDRESS': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
export const players = (state = [], action) => {
  switch (action.type) {
    case 'SET_PLAYERS': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
export const bets = (state = [], action) => {
  switch (action.type) {
    case 'SET_BETS': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
export const web3 = (state = {}, action) => {
  switch (action.type) {
    case 'SET_WEB3': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
export const contractInstance = (state = {}, action) => {
  switch (action.type) {
    case 'SET_CONTRACTINSTANCE': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
export const showAlert = (state = false, action) => {
  switch (action.type) {
    case 'SHOW_ALERT': {
      return true;
    }
    case 'HIDE_ALERT': {
      return false;
    }
    default:
    {
      return state;
    }
  }
};
export const weiToEth = (state = 1000000000000000000, action) => {
  switch (action.type) {
    default:
    {
      return state;
    }
  }
};

export const message = (state = '', action) => {
  switch (action.type) {
    case 'SET_MESSAGE': {
      return action.payload;
    }

    default:
    {
      return state;
    }
  }
};

export const betSize = (state = 0, action) => {
  switch (action.type) {
    case 'SET_BETSIZE': {
      return action.payload;
    }

    default:
    {
      return state;
    }
  }
};
export const maxBetsVar = (state = 2, action) => {
  switch (action.type) {
    case 'SET_MAXBETSVAR': {
      return action.payload;
    }
    default:
    {
      return state;
    }
  }
};
