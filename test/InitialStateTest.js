var CryptoLotery = artifacts.require("../contracts/CryptoLotery.sol");

const BN = web3.utils.BN;

contract('CryptoLotery', (accounts) =>
{

  it("account[0] created smart contract", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let owner = await contract.owner.call();

    assert.equal(owner, accounts[0], "Owner return wrong address")
  });

  it("Initial value of maxBets=2", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let maxBets = await contract.maxBets.call();

    assert.equal(maxBets, 2, "Initial value of maxBets is wrong")
  });
  it("Initial value of currentBets=0", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let currentBets = await contract.currentBets.call();

    assert.equal(currentBets, 0, "Initial value of currentBets is wrong")
  });

  it("Initial value of pool=0", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let pool = await contract.pool.call();

    assert.equal(pool, 0, "Initial value of pool is wrong")
  });

  it("Initial value of lastRandom=0", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let lastRandom = await contract.lastRandom.call();

    assert.equal(lastRandom, 0, "Initial value of lastRandom is wrong")
  });
  it("should fail beacuse nonce is not public ", async () =>
  {
    let contract = await CryptoLotery.deployed();
    try
    {
      let nonce = await contract.nonce.call();
    } catch (e)
    {
      return true;
    }
    throw new Error("You got error, nonce is not public")
  });

  it("should fail because function does not exist on contract", async () =>
  {
    let contract = await CryptoLotery.deployed();
    try
    {
      await contract.randomFunc.call();
    } catch (e)
    {
      return true;
    }
    throw new Error("You got error this transaction should not exist")
  })

})
