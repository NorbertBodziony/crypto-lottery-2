/* eslint-disable */
var CryptoLotery = artifacts.require("../contracts/CryptoLotery.sol");

const BN = web3.utils.BN;

contract('CryptoLotery', (accounts) =>
{

  it("PlaceBet should update pool currentBets,pool,bets,players (1st bet)", async () =>
  {
    //initial currentBets and pool =0 
    //bets and players are empty
    let contract = await CryptoLotery.deployed();

    let ETH = 0.01
    await contract.placeBet({ from: accounts[0], value: web3.utils.toWei(ETH.toString(), 'ether') })
    let pool = await contract.pool.call();

    assert.equal(pool, web3.utils.toWei(ETH.toString(), 'ether'), "value of pool is wrong")
    let currentBets = await contract.currentBets.call();
    assert.equal(currentBets, 1, "value of currentBets is wrong")
    let players = await contract.players.call(web3.utils.toBN(0));
    assert.equal(players, accounts[0], "player hasn't added")
    let bet = await contract.bets.call(web3.utils.toBN(0));
    assert.equal(bet, web3.utils.toWei(ETH.toString(), 'ether'), "bet hasn't added")
  })

  it("PlaceBet should update pool currentBets,pool,bets,players (2st bet)", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let ETH = 0.01
    let initialBalance= await web3.eth.getBalance(accounts[0])
    await contract.placeBet({ from: accounts[0], value: web3.utils.toWei(ETH.toString(), 'ether') })
    let pool = await contract.pool.call();
    
    assert.equal(pool, 0, "value of pool is wrong")
    let currentBets = await contract.currentBets.call();
    assert.equal(currentBets, 0, "value of currentBets is wrong")

    try
    {
      let players = await contract.players.call(web3.utils.toBN(0));
      throw new Error("You got error ,this call should throw exception")
    } catch (e)
    {
    }
    try
    {
      let bet = await contract.bets.call(web3.utils.toBN(0));
      throw new Error("You got error, this call should throw exception")
    } catch (e)
    {    
    }
    let finalBalance=await web3.eth.getBalance(accounts[0])
    assert(initialBalance< finalBalance, "contract didnt transfer funds")
  })

  it("lastWinner should be updated", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let lastWinner = await contract.lastWinner.call();

    assert.equal(lastWinner, accounts[0], "lastWinner returned wrong address")
  });

  it("lastRandom should be updated", async () =>
  {
    let contract = await CryptoLotery.deployed();
    let lastRandom = await contract.lastRandom.call();

    assert(lastRandom!= 0, "lastRandom hasn't updated")
  });
})
