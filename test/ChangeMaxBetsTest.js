/* eslint-disable */
const CryptoLotery = artifacts.require('../contracts/CryptoLotery.sol');

const { BN } = web3.utils;
const betTest = 0.01;
contract('CryptoLotery', (accounts) => {
  it('Overflow maxBets', async () => {
    const contract = await CryptoLotery.deployed();
    const maxBets = 2 ** 8;

    try {
      await contract.setLotteryCount(web3.utils.toBN(maxBets), { from: accounts[0] });
      throw new Error('this function should throw exception');
    } catch (e) {
      return true;
    }
  });

  it('Change maxBets to 5', async () => {
    const contract = await CryptoLotery.deployed();
    let maxBets = 5;

    await contract.setLotteryCount(web3.utils.toBN(maxBets), { from: accounts[0] });
    maxBets = await contract.maxBets.call();

    assert.equal(maxBets, 5, 'value of maxBets is wrong');
  });

  it('Place 4 Bets', async () => {
    // initial currentBets and pool =0
    // bets and players are empty

    const contract = await CryptoLotery.deployed();

    await contract.placeBet({ from: accounts[0], value: web3.utils.toWei(betTest.toString(), 'ether') });
    await contract.placeBet({ from: accounts[0], value: web3.utils.toWei(betTest.toString(), 'ether') });
    await contract.placeBet({ from: accounts[0], value: web3.utils.toWei(betTest.toString(), 'ether') });
    await contract.placeBet({ from: accounts[0], value: web3.utils.toWei(betTest.toString(), 'ether') });
  });
  it('Contract store players and best', async () => {
    // initial currentBets and pool =0
    // bets and players are empty

    const contract = await CryptoLotery.deployed();
    const currentBets = await contract.currentBets.call();
    const players = [];
    const bets = [];
    for (let i = 0; i < currentBets; i++) {
      players.push(await contract.players.call(web3.utils.toBN(i)));
      bets.push(await contract.bets.call(web3.utils.toBN(i)));
    }
    assert.equal(players.length, 4, 'contract does not store all player');
    assert.equal(bets.length, 4, 'contract does not store all bets');
  });
  it('value of currentBets should equal 4', async () => {
    const contract = await CryptoLotery.deployed();
    const currentBets = await contract.currentBets.call();

    assert.equal(currentBets, 4, 'value of currentBets is wrong');
  });

  it('value of pool should be equal to all bets ', async () => {
    const contract = await CryptoLotery.deployed();
    const pool = await contract.pool.call();

    assert.equal(pool, 4 * web3.utils.toWei(betTest.toString()), 'value of pool is wrong');
  });

  it('Place last bet', async () => {
    // initial currentBets and pool =0
    // bets and players are empty
    const contract = await CryptoLotery.deployed();
    await contract.placeBet({ from: accounts[0], value: web3.utils.toWei(betTest.toString(), 'ether') });
  });

  it('lastWinner should be updated', async () => {
    const contract = await CryptoLotery.deployed();
    const lastWinner = await contract.lastWinner.call();

    assert.equal(lastWinner, accounts[0], 'lastWinner returned wrong address');
  });

  it('lastRandom should be updated', async () => {
    const contract = await CryptoLotery.deployed();
    const lastRandom = await contract.lastRandom.call();

    assert(lastRandom != 0, "lastRandom hasn't updated");
  });
  it('Pool = 0', async () => {
    const contract = await CryptoLotery.deployed();
    const pool = await contract.pool.call();

    assert.equal(pool, 0, 'value of pool is wrong');
  });

  it('currentBets = 0', async () => {
    const contract = await CryptoLotery.deployed();
    const currentBets = await contract.currentBets.call();

    assert.equal(currentBets, 0, 'value of currentBets is wrong');
  });
});
