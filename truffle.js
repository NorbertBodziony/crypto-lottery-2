// *************
// This Gist fixes the truffle.js in the tutorial on this link:
// https://medium.com/coinmonks/create-a-sports-betting-dapp-on-the-ethereum-blockchain-part-1-1f69f908b939
// --
// The code in the Medium article stops at dry run and never actually deploys the contract to Ropsten.
// This is almost certainly because of a change in how Truffle works.

var WalletProvider = require("truffle-wallet-provider");
// ***CHANGE -> using HDWalletProvider
var HDWalletProvider = require("truffle-hdwallet-provider");
// ***CHANGE -> we store the private key as a string, not as a Buffer
const MNEMONIC='3fe3b8cef4293a872b26f0d3c87eeda7628cfa3215286f3cbbd9194425d08d33';
const Wallet = require('ethereumjs-wallet');
// ***CHANGE -> the following 3 lines had to be commented out
//var ropstenPrivateKey = new Buffer("YOUR-PRIVATE-KEY","hex");
//var ropstenWallet = Wallet.fromPrivateKey(ropstenPrivateKey);
//var ropstenProvider = new WalletProvider(ropstenWallet, "https://ropsten.infura.io/v3/INFURA-KEY");
module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*", // Match any network id
    },
    ropsten: {
      //provider: ropstenProvider,
      // ***CHANGE -> using HDWalletProvider, note that we don't have /v3/ in the Infura link
      provider: function() {
        return new HDWalletProvider(MNEMONIC, "https://ropsten.infura.io/v3/2bcdd032b65c40f8b13169f8cb45fc13")
      },
      // ***CHANGE -> lowered gas
      gas: 4000000,
      network_id: 3
    }
  }
};

